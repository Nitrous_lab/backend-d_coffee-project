import { IsNotEmpty } from 'class-validator';

export class CreateSummarysalaryDto {
  @IsNotEmpty()
  workhour: Date;

  @IsNotEmpty()
  salary: number;
}
