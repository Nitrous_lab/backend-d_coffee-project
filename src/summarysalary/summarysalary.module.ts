import { Module } from '@nestjs/common';
import { SummarysalaryService } from './summarysalary.service';
import { SummarysalaryController } from './summarysalary.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Summarysalary } from './entities/summarysalary.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Summarysalary])],
  controllers: [SummarysalaryController],
  providers: [SummarysalaryService],
})
export class SummarysalaryModule {}
