import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateSummarysalaryDto } from './dto/create-summarysalary.dto';
import { UpdateSummarysalaryDto } from './dto/update-summarysalary.dto';
import { Summarysalary } from './entities/summarysalary.entity';

@Injectable()
export class SummarysalaryService {
  constructor(
    @InjectRepository(Summarysalary)
    private summarysalaryRepository: Repository<Summarysalary>,
  ) {}
  create(createSummarysalaryDto: CreateSummarysalaryDto) {
    return this.summarysalaryRepository.save(createSummarysalaryDto);
  }

  findAll() {
    return this.summarysalaryRepository.find();
  }

  findOne(id: number) {
    return this.summarysalaryRepository.findOne({ where: { id } });
  }

  async update(id: number, updateSummarysalaryDto: UpdateSummarysalaryDto) {
    const summarysalary = await this.summarysalaryRepository.findOneBy({ id });
    if (!summarysalary) {
      throw new NotFoundException();
    }
    const updateSummarysalary = { ...summarysalary, ...updateSummarysalaryDto };
    return this.summarysalaryRepository.save(updateSummarysalary);
  }

  async remove(id: number) {
    const summarysalary = await this.summarysalaryRepository.findOneBy({ id });
    if (!summarysalary) {
      throw new NotFoundException();
    }
    return this.summarysalaryRepository.softRemove(summarysalary);
  }
}
